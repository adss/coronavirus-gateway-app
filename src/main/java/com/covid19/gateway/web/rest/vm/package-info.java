/**
 * View Models used by Spring MVC REST controllers.
 */
package com.covid19.gateway.web.rest.vm;
