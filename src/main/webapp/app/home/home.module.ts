import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoronavirusGatewayAppSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [CoronavirusGatewayAppSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class CoronavirusGatewayAppHomeModule {}
