import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { CoronavirusGatewayAppSharedModule } from 'app/shared/shared.module';
import { CoronavirusGatewayAppCoreModule } from 'app/core/core.module';
import { CoronavirusGatewayAppAppRoutingModule } from './app-routing.module';
import { CoronavirusGatewayAppHomeModule } from './home/home.module';
import { CoronavirusGatewayAppEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    CoronavirusGatewayAppSharedModule,
    CoronavirusGatewayAppCoreModule,
    CoronavirusGatewayAppHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    CoronavirusGatewayAppEntityModule,
    CoronavirusGatewayAppAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class CoronavirusGatewayAppAppModule {}
