import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'tracker',
        loadChildren: () => import('./trackerApp/tracker/tracker.module').then(m => m.TrackerAppTrackerModule),
      },
      {
        path: 'tracker-state',
        loadChildren: () => import('./trackerApp/tracker-state/tracker-state.module').then(m => m.TrackerAppTrackerStateModule),
      },
      {
        path: 'vaccine',
        loadChildren: () => import('./vaccineApp/vaccine/vaccine.module').then(m => m.VaccineAppVaccineModule),
      },
      {
        path: 'country',
        loadChildren: () => import('./vaccineApp/country/country.module').then(m => m.VaccineAppCountryModule),
      },
      {
        path: 'vaccine-type',
        loadChildren: () => import('./vaccineApp/vaccine-type/vaccine-type.module').then(m => m.VaccineAppVaccineTypeModule),
      },
      {
        path: 'phase',
        loadChildren: () => import('./vaccineApp/phase/phase.module').then(m => m.VaccineAppPhaseModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class CoronavirusGatewayAppEntityModule {}
