import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoronavirusGatewayAppSharedModule } from 'app/shared/shared.module';
import { VaccineTypeComponent } from './vaccine-type.component';
import { VaccineTypeDetailComponent } from './vaccine-type-detail.component';
import { VaccineTypeUpdateComponent } from './vaccine-type-update.component';
import { VaccineTypeDeleteDialogComponent } from './vaccine-type-delete-dialog.component';
import { vaccineTypeRoute } from './vaccine-type.route';

@NgModule({
  imports: [CoronavirusGatewayAppSharedModule, RouterModule.forChild(vaccineTypeRoute)],
  declarations: [VaccineTypeComponent, VaccineTypeDetailComponent, VaccineTypeUpdateComponent, VaccineTypeDeleteDialogComponent],
  entryComponents: [VaccineTypeDeleteDialogComponent],
})
export class VaccineAppVaccineTypeModule {}
