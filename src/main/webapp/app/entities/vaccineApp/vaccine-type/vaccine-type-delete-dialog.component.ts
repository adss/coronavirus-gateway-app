import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVaccineType } from 'app/shared/model/vaccineApp/vaccine-type.model';
import { VaccineTypeService } from './vaccine-type.service';

@Component({
  templateUrl: './vaccine-type-delete-dialog.component.html',
})
export class VaccineTypeDeleteDialogComponent {
  vaccineType?: IVaccineType;

  constructor(
    protected vaccineTypeService: VaccineTypeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.vaccineTypeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('vaccineTypeListModification');
      this.activeModal.close();
    });
  }
}
