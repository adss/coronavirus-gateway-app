import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IVaccineType, VaccineType } from 'app/shared/model/vaccineApp/vaccine-type.model';
import { VaccineTypeService } from './vaccine-type.service';
import { VaccineTypeComponent } from './vaccine-type.component';
import { VaccineTypeDetailComponent } from './vaccine-type-detail.component';
import { VaccineTypeUpdateComponent } from './vaccine-type-update.component';

@Injectable({ providedIn: 'root' })
export class VaccineTypeResolve implements Resolve<IVaccineType> {
  constructor(private service: VaccineTypeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVaccineType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((vaccineType: HttpResponse<VaccineType>) => {
          if (vaccineType.body) {
            return of(vaccineType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new VaccineType());
  }
}

export const vaccineTypeRoute: Routes = [
  {
    path: '',
    component: VaccineTypeComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'VaccineTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VaccineTypeDetailComponent,
    resolve: {
      vaccineType: VaccineTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VaccineTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VaccineTypeUpdateComponent,
    resolve: {
      vaccineType: VaccineTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VaccineTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VaccineTypeUpdateComponent,
    resolve: {
      vaccineType: VaccineTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VaccineTypes',
    },
    canActivate: [UserRouteAccessService],
  },
];
