import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVaccineType } from 'app/shared/model/vaccineApp/vaccine-type.model';

type EntityResponseType = HttpResponse<IVaccineType>;
type EntityArrayResponseType = HttpResponse<IVaccineType[]>;

@Injectable({ providedIn: 'root' })
export class VaccineTypeService {
  public resourceUrl = SERVER_API_URL + 'services/vaccineapp/api/vaccine-types';

  constructor(protected http: HttpClient) {}

  create(vaccineType: IVaccineType): Observable<EntityResponseType> {
    return this.http.post<IVaccineType>(this.resourceUrl, vaccineType, { observe: 'response' });
  }

  update(vaccineType: IVaccineType): Observable<EntityResponseType> {
    return this.http.put<IVaccineType>(this.resourceUrl, vaccineType, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IVaccineType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVaccineType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
