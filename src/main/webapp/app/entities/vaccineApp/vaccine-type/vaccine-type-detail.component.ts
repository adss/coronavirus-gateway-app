import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IVaccineType } from 'app/shared/model/vaccineApp/vaccine-type.model';

@Component({
  selector: 'jhi-vaccine-type-detail',
  templateUrl: './vaccine-type-detail.component.html',
})
export class VaccineTypeDetailComponent implements OnInit {
  vaccineType: IVaccineType | null = null;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vaccineType }) => (this.vaccineType = vaccineType));
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }
}
