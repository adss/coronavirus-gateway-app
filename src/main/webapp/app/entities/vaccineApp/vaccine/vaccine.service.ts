import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVaccine } from 'app/shared/model/vaccineApp/vaccine.model';

type EntityResponseType = HttpResponse<IVaccine>;
type EntityArrayResponseType = HttpResponse<IVaccine[]>;

@Injectable({ providedIn: 'root' })
export class VaccineService {
  public resourceUrl = SERVER_API_URL + 'services/vaccineapp/api/vaccines';

  constructor(protected http: HttpClient) {}

  create(vaccine: IVaccine): Observable<EntityResponseType> {
    return this.http.post<IVaccine>(this.resourceUrl, vaccine, { observe: 'response' });
  }

  update(vaccine: IVaccine): Observable<EntityResponseType> {
    return this.http.put<IVaccine>(this.resourceUrl, vaccine, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IVaccine>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVaccine[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
