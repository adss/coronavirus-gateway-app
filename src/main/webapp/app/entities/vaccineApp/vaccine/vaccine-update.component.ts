import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IVaccine, Vaccine } from 'app/shared/model/vaccineApp/vaccine.model';
import { VaccineService } from './vaccine.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IVaccineType } from 'app/shared/model/vaccineApp/vaccine-type.model';
import { VaccineTypeService } from 'app/entities/vaccineApp/vaccine-type/vaccine-type.service';
import { ICountry } from 'app/shared/model/vaccineApp/country.model';
import { CountryService } from 'app/entities/vaccineApp/country/country.service';

type SelectableEntity = IVaccineType | ICountry;

@Component({
  selector: 'jhi-vaccine-update',
  templateUrl: './vaccine-update.component.html',
})
export class VaccineUpdateComponent implements OnInit {
  isSaving = false;
  vaccinetypes: IVaccineType[] = [];
  countries: ICountry[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    company: [],
    companyContentType: [],
    company2: [],
    company2ContentType: [],
    efficacy: [],
    dose: [],
    type: [],
    storage: [],
    description: [null, [Validators.required]],
    vaccineTypeId: [],
    countries: [],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected vaccineService: VaccineService,
    protected vaccineTypeService: VaccineTypeService,
    protected countryService: CountryService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vaccine }) => {
      this.updateForm(vaccine);

      this.vaccineTypeService
        .query({ filter: 'vaccine-is-null' })
        .pipe(
          map((res: HttpResponse<IVaccineType[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IVaccineType[]) => {
          if (!vaccine.vaccineTypeId) {
            this.vaccinetypes = resBody;
          } else {
            this.vaccineTypeService
              .find(vaccine.vaccineTypeId)
              .pipe(
                map((subRes: HttpResponse<IVaccineType>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IVaccineType[]) => (this.vaccinetypes = concatRes));
          }
        });

      this.countryService.query().subscribe((res: HttpResponse<ICountry[]>) => (this.countries = res.body || []));
    });
  }

  updateForm(vaccine: IVaccine): void {
    this.editForm.patchValue({
      id: vaccine.id,
      name: vaccine.name,
      company: vaccine.company,
      companyContentType: vaccine.companyContentType,
      company2: vaccine.company2,
      company2ContentType: vaccine.company2ContentType,
      efficacy: vaccine.efficacy,
      dose: vaccine.dose,
      type: vaccine.type,
      storage: vaccine.storage,
      description: vaccine.description,
      vaccineTypeId: vaccine.vaccineTypeId,
      countries: vaccine.countries,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('coronavirusGatewayApp.error', { message: err.message })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const vaccine = this.createFromForm();
    if (vaccine.id !== undefined) {
      this.subscribeToSaveResponse(this.vaccineService.update(vaccine));
    } else {
      this.subscribeToSaveResponse(this.vaccineService.create(vaccine));
    }
  }

  private createFromForm(): IVaccine {
    return {
      ...new Vaccine(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      companyContentType: this.editForm.get(['companyContentType'])!.value,
      company: this.editForm.get(['company'])!.value,
      company2ContentType: this.editForm.get(['company2ContentType'])!.value,
      company2: this.editForm.get(['company2'])!.value,
      efficacy: this.editForm.get(['efficacy'])!.value,
      dose: this.editForm.get(['dose'])!.value,
      type: this.editForm.get(['type'])!.value,
      storage: this.editForm.get(['storage'])!.value,
      description: this.editForm.get(['description'])!.value,
      vaccineTypeId: this.editForm.get(['vaccineTypeId'])!.value,
      countries: this.editForm.get(['countries'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVaccine>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: ICountry[], option: ICountry): ICountry {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
