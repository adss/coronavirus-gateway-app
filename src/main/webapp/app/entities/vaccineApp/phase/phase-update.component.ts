import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IPhase, Phase } from 'app/shared/model/vaccineApp/phase.model';
import { PhaseService } from './phase.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IVaccine } from 'app/shared/model/vaccineApp/vaccine.model';
import { VaccineService } from 'app/entities/vaccineApp/vaccine/vaccine.service';

@Component({
  selector: 'jhi-phase-update',
  templateUrl: './phase-update.component.html',
})
export class PhaseUpdateComponent implements OnInit {
  isSaving = false;
  vaccines: IVaccine[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    description: [null, [Validators.required, Validators.minLength(40), Validators.maxLength(500)]],
    image: [],
    imageContentType: [],
    vaccineId: [],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected phaseService: PhaseService,
    protected vaccineService: VaccineService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phase }) => {
      this.updateForm(phase);

      this.vaccineService.query().subscribe((res: HttpResponse<IVaccine[]>) => (this.vaccines = res.body || []));
    });
  }

  updateForm(phase: IPhase): void {
    this.editForm.patchValue({
      id: phase.id,
      name: phase.name,
      description: phase.description,
      image: phase.image,
      imageContentType: phase.imageContentType,
      vaccineId: phase.vaccineId,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('coronavirusGatewayApp.error', { message: err.message })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const phase = this.createFromForm();
    if (phase.id !== undefined) {
      this.subscribeToSaveResponse(this.phaseService.update(phase));
    } else {
      this.subscribeToSaveResponse(this.phaseService.create(phase));
    }
  }

  private createFromForm(): IPhase {
    return {
      ...new Phase(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      imageContentType: this.editForm.get(['imageContentType'])!.value,
      image: this.editForm.get(['image'])!.value,
      vaccineId: this.editForm.get(['vaccineId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhase>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IVaccine): any {
    return item.id;
  }
}
