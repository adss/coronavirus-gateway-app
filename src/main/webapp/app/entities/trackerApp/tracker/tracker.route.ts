import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITracker, Tracker } from 'app/shared/model/trackerApp/tracker.model';
import { TrackerService } from './tracker.service';
import { TrackerComponent } from './tracker.component';
import { TrackerDetailComponent } from './tracker-detail.component';

@Injectable({ providedIn: 'root' })
export class TrackerResolve implements Resolve<ITracker> {
  constructor(private service: TrackerService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITracker> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tracker: HttpResponse<Tracker>) => {
          if (tracker.body) {
            return of(tracker.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Tracker());
  }
}

export const trackerRoute: Routes = [
  {
    path: '',
    component: TrackerComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'Trackers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TrackerDetailComponent,
    resolve: {
      tracker: TrackerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Trackers',
    },
    canActivate: [UserRouteAccessService],
  },
];
