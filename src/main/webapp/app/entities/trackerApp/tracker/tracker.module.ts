import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoronavirusGatewayAppSharedModule } from 'app/shared/shared.module';
import { TrackerComponent } from './tracker.component';
import { TrackerDetailComponent } from './tracker-detail.component';
import { trackerRoute } from './tracker.route';

@NgModule({
  imports: [CoronavirusGatewayAppSharedModule, RouterModule.forChild(trackerRoute)],
  declarations: [TrackerComponent, TrackerDetailComponent],
})
export class TrackerAppTrackerModule {}
