import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITracker } from 'app/shared/model/trackerApp/tracker.model';

type EntityResponseType = HttpResponse<ITracker>;
type EntityArrayResponseType = HttpResponse<ITracker[]>;

@Injectable({ providedIn: 'root' })
export class TrackerService {
  public resourceUrl = SERVER_API_URL + 'services/trackerapp/api/trackers';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITracker>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITracker[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(tracker: ITracker): ITracker {
    const copy: ITracker = Object.assign({}, tracker, {
      consultedAt: tracker.consultedAt && tracker.consultedAt.isValid() ? tracker.consultedAt.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.consultedAt = res.body.consultedAt ? moment(res.body.consultedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tracker: ITracker) => {
        tracker.consultedAt = tracker.consultedAt ? moment(tracker.consultedAt) : undefined;
      });
    }
    return res;
  }
}
