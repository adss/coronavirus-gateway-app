import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITrackerState, TrackerState } from 'app/shared/model/trackerApp/tracker-state.model';
import { TrackerStateService } from './tracker-state.service';
import { TrackerStateComponent } from './tracker-state.component';
import { TrackerStateDetailComponent } from './tracker-state-detail.component';

@Injectable({ providedIn: 'root' })
export class TrackerStateResolve implements Resolve<ITrackerState> {
  constructor(private service: TrackerStateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITrackerState> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((trackerState: HttpResponse<TrackerState>) => {
          if (trackerState.body) {
            return of(trackerState.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TrackerState());
  }
}

export const trackerStateRoute: Routes = [
  {
    path: '',
    component: TrackerStateComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'TrackerStates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TrackerStateDetailComponent,
    resolve: {
      trackerState: TrackerStateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'TrackerStates',
    },
    canActivate: [UserRouteAccessService],
  },
];
