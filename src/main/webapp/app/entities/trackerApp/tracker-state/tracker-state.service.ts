import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITrackerState } from 'app/shared/model/trackerApp/tracker-state.model';

type EntityResponseType = HttpResponse<ITrackerState>;
type EntityArrayResponseType = HttpResponse<ITrackerState[]>;

@Injectable({ providedIn: 'root' })
export class TrackerStateService {
  public resourceUrl = SERVER_API_URL + 'services/trackerapp/api/tracker-states';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITrackerState>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITrackerState[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
}
