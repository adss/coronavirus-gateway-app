import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoronavirusGatewayAppSharedModule } from 'app/shared/shared.module';
import { TrackerStateComponent } from './tracker-state.component';
import { TrackerStateDetailComponent } from './tracker-state-detail.component';
import { trackerStateRoute } from './tracker-state.route';

@NgModule({
  imports: [CoronavirusGatewayAppSharedModule, RouterModule.forChild(trackerStateRoute)],
  declarations: [TrackerStateComponent, TrackerStateDetailComponent],
})
export class TrackerAppTrackerStateModule {}
