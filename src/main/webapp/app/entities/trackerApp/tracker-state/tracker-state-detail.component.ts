import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITrackerState } from 'app/shared/model/trackerApp/tracker-state.model';

@Component({
  selector: 'jhi-tracker-state-detail',
  templateUrl: './tracker-state-detail.component.html',
})
export class TrackerStateDetailComponent implements OnInit {
  trackerState: ITrackerState | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ trackerState }) => (this.trackerState = trackerState));
  }

  previousState(): void {
    window.history.back();
  }
}
