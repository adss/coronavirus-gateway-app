export interface IVaccineType {
  id?: number;
  name?: string;
  typeImageContentType?: string;
  typeImage?: any;
}

export class VaccineType implements IVaccineType {
  constructor(public id?: number, public name?: string, public typeImageContentType?: string, public typeImage?: any) {}
}
