import { IVaccine } from 'app/shared/model/vaccineApp/vaccine.model';

export interface ICountry {
  id?: number;
  name?: string;
  flagContentType?: string;
  flag?: any;
  vaccines?: IVaccine[];
}

export class Country implements ICountry {
  constructor(public id?: number, public name?: string, public flagContentType?: string, public flag?: any, public vaccines?: IVaccine[]) {}
}
