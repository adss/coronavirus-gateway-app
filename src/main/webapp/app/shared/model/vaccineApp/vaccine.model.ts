import { IPhase } from 'app/shared/model/vaccineApp/phase.model';
import { ICountry } from 'app/shared/model/vaccineApp/country.model';

export interface IVaccine {
  id?: number;
  name?: string;
  companyContentType?: string;
  company?: any;
  company2ContentType?: string;
  company2?: any;
  efficacy?: number;
  dose?: string;
  type?: string;
  storage?: string;
  description?: any;
  vaccineTypeName?: string;
  vaccineTypeId?: number;
  phases?: IPhase[];
  countries?: ICountry[];
}

export class Vaccine implements IVaccine {
  constructor(
    public id?: number,
    public name?: string,
    public companyContentType?: string,
    public company?: any,
    public company2ContentType?: string,
    public company2?: any,
    public efficacy?: number,
    public dose?: string,
    public type?: string,
    public storage?: string,
    public description?: any,
    public vaccineTypeName?: string,
    public vaccineTypeId?: number,
    public phases?: IPhase[],
    public countries?: ICountry[]
  ) {}
}
