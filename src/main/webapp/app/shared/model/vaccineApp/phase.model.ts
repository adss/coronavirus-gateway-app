export interface IPhase {
  id?: number;
  name?: string;
  description?: string;
  imageContentType?: string;
  image?: any;
  vaccineId?: number;
}

export class Phase implements IPhase {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public imageContentType?: string,
    public image?: any,
    public vaccineId?: number
  ) {}
}
