import { Moment } from 'moment';
import { ITrackerState } from 'app/shared/model/trackerApp/tracker-state.model';

export interface ITracker {
  id?: number;
  consultedAt?: Moment;
  consultedBy?: string;
  trackerStates?: ITrackerState[];
}

export class Tracker implements ITracker {
  constructor(public id?: number, public consultedAt?: Moment, public consultedBy?: string, public trackerStates?: ITrackerState[]) {}
}
