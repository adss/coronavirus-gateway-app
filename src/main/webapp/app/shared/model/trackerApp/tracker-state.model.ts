export interface ITrackerState {
  id?: number;
  phase?: string;
  totalVaccines?: number;
  phaseDetail?: string;
  trackerId?: number;
}

export class TrackerState implements ITrackerState {
  constructor(
    public id?: number,
    public phase?: string,
    public totalVaccines?: number,
    public phaseDetail?: string,
    public trackerId?: number
  ) {}
}
