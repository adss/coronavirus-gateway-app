import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoronavirusGatewayAppTestModule } from '../../../../test.module';
import { TrackerStateDetailComponent } from 'app/entities/trackerApp/tracker-state/tracker-state-detail.component';
import { TrackerState } from 'app/shared/model/trackerApp/tracker-state.model';

describe('Component Tests', () => {
  describe('TrackerState Management Detail Component', () => {
    let comp: TrackerStateDetailComponent;
    let fixture: ComponentFixture<TrackerStateDetailComponent>;
    const route = ({ data: of({ trackerState: new TrackerState(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronavirusGatewayAppTestModule],
        declarations: [TrackerStateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TrackerStateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TrackerStateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load trackerState on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.trackerState).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
