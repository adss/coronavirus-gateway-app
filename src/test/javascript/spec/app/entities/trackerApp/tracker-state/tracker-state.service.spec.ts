import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TrackerStateService } from 'app/entities/trackerApp/tracker-state/tracker-state.service';
import { ITrackerState, TrackerState } from 'app/shared/model/trackerApp/tracker-state.model';

describe('Service Tests', () => {
  describe('TrackerState Service', () => {
    let injector: TestBed;
    let service: TrackerStateService;
    let httpMock: HttpTestingController;
    let elemDefault: ITrackerState;
    let expectedResult: ITrackerState | ITrackerState[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(TrackerStateService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new TrackerState(0, 'AAAAAAA', 0, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should return a list of TrackerState', () => {
        const returnedFromService = Object.assign(
          {
            phase: 'BBBBBB',
            totalVaccines: 1,
            phaseDetail: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
