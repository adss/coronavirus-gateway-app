import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { TrackerService } from 'app/entities/trackerApp/tracker/tracker.service';
import { ITracker, Tracker } from 'app/shared/model/trackerApp/tracker.model';

describe('Service Tests', () => {
  describe('Tracker Service', () => {
    let injector: TestBed;
    let service: TrackerService;
    let httpMock: HttpTestingController;
    let elemDefault: ITracker;
    let expectedResult: ITracker | ITracker[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(TrackerService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Tracker(0, currentDate, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            consultedAt: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should return a list of Tracker', () => {
        const returnedFromService = Object.assign(
          {
            consultedAt: currentDate.format(DATE_TIME_FORMAT),
            consultedBy: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            consultedAt: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
