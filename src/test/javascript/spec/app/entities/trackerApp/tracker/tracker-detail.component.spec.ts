import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoronavirusGatewayAppTestModule } from '../../../../test.module';
import { TrackerDetailComponent } from 'app/entities/trackerApp/tracker/tracker-detail.component';
import { Tracker } from 'app/shared/model/trackerApp/tracker.model';

describe('Component Tests', () => {
  describe('Tracker Management Detail Component', () => {
    let comp: TrackerDetailComponent;
    let fixture: ComponentFixture<TrackerDetailComponent>;
    const route = ({ data: of({ tracker: new Tracker(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronavirusGatewayAppTestModule],
        declarations: [TrackerDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(TrackerDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TrackerDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load tracker on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.tracker).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
