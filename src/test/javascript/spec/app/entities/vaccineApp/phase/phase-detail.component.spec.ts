import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { CoronavirusGatewayAppTestModule } from '../../../../test.module';
import { PhaseDetailComponent } from 'app/entities/vaccineApp/phase/phase-detail.component';
import { Phase } from 'app/shared/model/vaccineApp/phase.model';

describe('Component Tests', () => {
  describe('Phase Management Detail Component', () => {
    let comp: PhaseDetailComponent;
    let fixture: ComponentFixture<PhaseDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ phase: new Phase(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronavirusGatewayAppTestModule],
        declarations: [PhaseDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(PhaseDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PhaseDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load phase on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.phase).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
