import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { CoronavirusGatewayAppTestModule } from '../../../../test.module';
import { VaccineTypeDetailComponent } from 'app/entities/vaccineApp/vaccine-type/vaccine-type-detail.component';
import { VaccineType } from 'app/shared/model/vaccineApp/vaccine-type.model';

describe('Component Tests', () => {
  describe('VaccineType Management Detail Component', () => {
    let comp: VaccineTypeDetailComponent;
    let fixture: ComponentFixture<VaccineTypeDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ vaccineType: new VaccineType(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronavirusGatewayAppTestModule],
        declarations: [VaccineTypeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(VaccineTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VaccineTypeDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load vaccineType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.vaccineType).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
