import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { CoronavirusGatewayAppTestModule } from '../../../../test.module';
import { VaccineTypeUpdateComponent } from 'app/entities/vaccineApp/vaccine-type/vaccine-type-update.component';
import { VaccineTypeService } from 'app/entities/vaccineApp/vaccine-type/vaccine-type.service';
import { VaccineType } from 'app/shared/model/vaccineApp/vaccine-type.model';

describe('Component Tests', () => {
  describe('VaccineType Management Update Component', () => {
    let comp: VaccineTypeUpdateComponent;
    let fixture: ComponentFixture<VaccineTypeUpdateComponent>;
    let service: VaccineTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronavirusGatewayAppTestModule],
        declarations: [VaccineTypeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(VaccineTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VaccineTypeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VaccineTypeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new VaccineType(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new VaccineType();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
