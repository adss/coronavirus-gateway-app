import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CoronavirusGatewayAppTestModule } from '../../../../test.module';
import { MockEventManager } from '../../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../../helpers/mock-active-modal.service';
import { VaccineTypeDeleteDialogComponent } from 'app/entities/vaccineApp/vaccine-type/vaccine-type-delete-dialog.component';
import { VaccineTypeService } from 'app/entities/vaccineApp/vaccine-type/vaccine-type.service';

describe('Component Tests', () => {
  describe('VaccineType Management Delete Component', () => {
    let comp: VaccineTypeDeleteDialogComponent;
    let fixture: ComponentFixture<VaccineTypeDeleteDialogComponent>;
    let service: VaccineTypeService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronavirusGatewayAppTestModule],
        declarations: [VaccineTypeDeleteDialogComponent],
      })
        .overrideTemplate(VaccineTypeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VaccineTypeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VaccineTypeService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
