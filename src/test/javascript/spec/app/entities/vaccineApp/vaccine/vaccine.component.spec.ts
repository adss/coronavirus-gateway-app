import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { CoronavirusGatewayAppTestModule } from '../../../../test.module';
import { VaccineComponent } from 'app/entities/vaccineApp/vaccine/vaccine.component';
import { VaccineService } from 'app/entities/vaccineApp/vaccine/vaccine.service';
import { Vaccine } from 'app/shared/model/vaccineApp/vaccine.model';

describe('Component Tests', () => {
  describe('Vaccine Management Component', () => {
    let comp: VaccineComponent;
    let fixture: ComponentFixture<VaccineComponent>;
    let service: VaccineService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronavirusGatewayAppTestModule],
        declarations: [VaccineComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(VaccineComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VaccineComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VaccineService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Vaccine(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.vaccines && comp.vaccines[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Vaccine(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.vaccines && comp.vaccines[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
