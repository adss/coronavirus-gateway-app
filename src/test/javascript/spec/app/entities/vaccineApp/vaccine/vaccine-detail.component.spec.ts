import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { CoronavirusGatewayAppTestModule } from '../../../../test.module';
import { VaccineDetailComponent } from 'app/entities/vaccineApp/vaccine/vaccine-detail.component';
import { Vaccine } from 'app/shared/model/vaccineApp/vaccine.model';

describe('Component Tests', () => {
  describe('Vaccine Management Detail Component', () => {
    let comp: VaccineDetailComponent;
    let fixture: ComponentFixture<VaccineDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ vaccine: new Vaccine(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoronavirusGatewayAppTestModule],
        declarations: [VaccineDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(VaccineDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VaccineDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load vaccine on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.vaccine).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
